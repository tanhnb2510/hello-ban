<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/tasks',[\App\Http\Controllers\TaskController::class,'index'])->name('tasks.index');

Route::post('tasks/store',[\App\Http\Controllers\TaskController::class,'store'])->name('tasks.store')->middleware('auth');

Route::get('tasks/create',[\App\Http\Controllers\TaskController::class,'create'])->name('tasks.create')->middleware('auth');

Route::put('tasks/update/{id}',[\App\Http\Controllers\TaskController::class,'update'])->name('tasks.update')->middleware('auth');

Route::get('tasks/edit/{id}',[\App\Http\Controllers\TaskController::class,'edit'])->name('tasks.edit')->middleware('auth');

Route::get('tasks/{id}',[\App\Http\Controllers\TaskController::class,'show'])->name('tasks.show');


Route::get('works',[\App\Http\Controllers\WorkController::class,'index'])->name('works.index');

Route::post('works/store',[\App\Http\Controllers\WorkController::class,'store'])->name('works.store');

Route::get('/shops',[\App\Http\Controllers\ShopController::class,'index'])->name('shops.index');

Route::post('shops/store',[\App\Http\Controllers\ShopController::class,'store'])->name('shops.store')->middleware('auth');

Route::get('shops/create',[\App\Http\Controllers\ShopController::class,'create'])->name('shops.create');

Route::get('shops/{id}',[\App\Http\Controllers\ShopController::class,'show'])->name('shops.show');

Route::put('shops/update/{id}',[\App\Http\Controllers\ShopController::class,'update'])->name('shops.update')->middleware('auth');

Route::get('shops/edit/{id}',[\App\Http\Controllers\ShopController::class,'edit'])->name('shops.edit');

Route::get('posts/{id}',[\App\Http\Controllers\PostController::class,'show'])->name('posts.show');
