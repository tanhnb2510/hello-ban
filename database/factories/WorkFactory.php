<?php

namespace Database\Factories;

use App\Models\Work;
use Illuminate\Database\Eloquent\Factories\Factory;

class WorkFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model =Work::class;
    public function definition()
    {
        return [
            'name' =>$this->faker->name,
            'content' =>$this->faker->text
        ];
    }
}
