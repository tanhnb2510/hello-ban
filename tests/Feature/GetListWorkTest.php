<?php

namespace Tests\Feature;

use App\Models\Work;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetListWorkTest extends TestCase
{
    /** @test  */
    public function user_can_get_list_work()
    {
        $works = Work::factory()->create();
        $response = $this->get($this->getListWorkRoute());

        $response->assertStatus(200);
        $response->assertViewIs('works.index');
        $response->assertSee($works->name);
    }

    public function getListWorkRoute()
    {
        return route('works.index');
    }
}
