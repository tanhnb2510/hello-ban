<?php

namespace Tests\Feature;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Routing\Route;
use Tests\TestCase;

class GetListTaskTest extends TestCase
{

    /** @test  */
    public function user_can_gell_all_task()
    {
        $tasks = Task::factory()->create();
        $response = $this->get($this->getListTaskRoute());

        $response->assertViewIs('tasks.index');
        $response->assertSee($tasks->name);
        $response->assertStatus(Response::HTTP_OK);
    }

    public function getListTaskRoute()
    {
        return route('tasks.index');
    }
}
