<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{

    use WithFaker;
    /**  @test  */
    public function authenticated_user_can_update_task_test()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $taskUpdate =Task::factory()->make()->toArray();
        $response = $this->put($this->getUpdateTaskRoute($task->id),$taskUpdate);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('tasks',$taskUpdate);
        $response->assertRedirect(route('tasks.index'));
    }

    /** @test */
    public function unauthenticated_can_not_update_task()
    {
        $tasks =Task::factory()->create();
        $taskUpdate = Task::factory()->make()->toArray();
        $response = $this->put($this->getUpdateTaskRoute($tasks->id),$taskUpdate);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_see_view_update_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->get($this->getViewUpdateTaskRoute($task->id));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.edit');
    }

    /** @test */
    public function unauthenticated_user_can_not_see_view_update_task()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getViewUpdateTaskRoute($task->id));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_not_update_task_if_name_is_null()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $taskUpdate = Task::factory()->make([
            'name' => null
        ])->toArray();
        $response = $this->put($this->getUpdateTaskRoute($task->id),$taskUpdate);

        $response->assertSessionHasErrors('name');
    }
    public function getUpdateTaskRoute($id)
    {
        return route('tasks.update',['id' => $id]);
    }

    public function getViewUpdateTaskRoute($id)
    {
        return route('tasks.edit',['id' => $id]);
    }
}
