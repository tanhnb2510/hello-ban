<?php

namespace Tests\Feature\shop;

use App\Models\Shop;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetShowShopTest extends TestCase
{
    /** @test  */
    public function user_can_see_show_show()
    {
        $shop = Shop::factory()->create();
        $response = $this->get($this->getShowShopRoute($shop->id));

        $response->assertStatus(200);
        $response->assertViewIs('shops.show');
        $response->assertSee($shop->name);
    }

    public function getShowShopRoute($id)
    {
        return route('shops.show',['id' => $id]);
    }
}
