<?php

namespace Tests\Feature\shop;

use App\Models\Shop;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateNewShopTest extends TestCase
{
    use WithFaker;
    /** @test */
    public function authenticated_user_can_create_new_shop()
    {
        $this->actingAs(User::factory()->create());
        $shopUpdate =[
            'name'=>$this->faker->name,
            'content' =>$this->faker->text
        ];
        $response = $this->post($this->getCreateShopRoute(),$shopUpdate);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('shop',$shopUpdate);
        $response->assertRedirect(route('shops.index'));
    }
    /** @test */
    public function unauthenticate_can_not_create_new_shop()
    {
        $shopUpdate =[
            'name'=>$this->faker->name,
            'content' =>$this->faker->text
        ];
        $response = $this->post($this->getCreateShopRoute(),$shopUpdate);

        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_see_view_create_shop()
    {
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getViewCreateShopRoute());

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('shops.create');
    }

    /** @test */
    public function authenticated_user_can_not_create_shop_if_name_is_null()
    {
        $this->actingAs(User::factory()->create());
        $shopUpdate =[
            'name'=>null,
            'content' =>$this->faker->text
        ];
        $response = $this->post($this->getCreateShopRoute(),$shopUpdate);

        $response->assertSessionHasErrors('name');
    }

    public function getCreateShopRoute()
    {
        return route('shops.store');
    }

    public function getViewCreateShopRoute()
    {
        return route('shops.create');
    }
}
























