<?php

namespace Tests\Feature\shop;

use App\Models\Shop;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateShopTest extends TestCase
{
    use WithFaker;
    /** @test  */
    public function authenticated_user_can_update_shop()
    {
        $this->actingAs(User::factory()->create());
        $shop = Shop::factory()->create();
        $shopUpdate = [
            'name' => $this->faker->name,
            'content' => $this->faker->text
        ];
        $response = $this->put($this->getUpdateShopRoute($shop->id),$shopUpdate);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('shop',$shopUpdate);
        $response->assertRedirect(route('shops.index'));
    }

    /** @test  */
    public function unauthenticated_user_can_not_update_shop()
    {
        $shop = Shop::factory()->create();
        $shopUpdate = [
            'name' => $this->faker->name,
            'content' => $this->faker->text
        ];
        $response = $this->put($this->getUpdateShopRoute($shop->id),$shopUpdate);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test  */
    public function authenticated_user_can_see_update_shop()
    {
        $this->actingAs(User::factory()->create());
        $shop = Shop::factory()->create();
        $response = $this->get($this->getViewUpdateShopRoute($shop->id));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('shops.update');
    }

    public function getUpdateShopRoute($id)
    {
        return route('shops.update',['id'=>$id]);
    }

    public function getViewUpdateShopRoute($id)
    {
        return route('shops.edit',['id'=>$id]);
    }
}
