<?php

namespace Tests\Feature\shop;

use App\Models\Shop;
use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetListShopTest extends TestCase
{
    /** @test  */
    public function user_can_get_all_shop()
    {
        $shops = Shop::factory()->create();
        $response = $this->get($this->getListShopRoute());

        $response->assertStatus(200);
        $response->assertViewIs('shops.index');
        $response->assertSee($shops->name);
    }

    public function getListShopRoute()
    {
        return route('shops.index');
    }
}
