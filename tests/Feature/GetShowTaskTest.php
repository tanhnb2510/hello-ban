<?php

namespace Tests\Feature;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetShowTaskTest extends TestCase
{
    /** @test  */
    public function authenticated_user_can_see_detail_task()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getShowTaskRoute($task->id));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.show');
        $response->assertSee('name');
    }

    public function getShowTaskRoute($id)
    {
        return route('tasks.show',['id'=> $id]);
    }
}
