<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateNewTaskTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_new_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create()->toArray();
        $response = $this->post($this->GetCreateTaskRoute(),$task);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('tasks',$task);
        $response->assertRedirect(route('tasks.index'));
    }

    /** @test */
    public function unauthenticated_user_can_not_new_task()
    {
        $task = Task::factory()->create();
        $response = $this->post($this->GetCreateTaskRoute());

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_see_view_create_task()
    {
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getCreateViewTaskRoute());

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.create');
    }

    /** @test */
    public function unauthenticated_user_Can_not_see_view_create_task()
    {
        $response = $this->get($this->getCreateViewTaskRoute());

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_not_create_new_task_if_name_is_null()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create([
            'name' => null
        ])->toArray();
        $response = $this->post($this->GetCreateTaskRoute(),$task);

        $response->assertSessionHasErrors('name');
    }



    public function GetCreateTaskRoute()
    {
        return route('tasks.store');
    }

    public function getCreateViewTaskRoute()
    {
        return route('tasks.create');
    }
}
