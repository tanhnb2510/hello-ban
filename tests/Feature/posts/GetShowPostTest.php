<?php

namespace Tests\Feature\posts;

use App\Models\Post;
use http\Client\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetShowPostTest extends TestCase
{
    /** @test */
    public function user_can_show_post()
    {
        $post = Post::factory()->create();
        $response = $this->get($this->getShowPostRoute($post->id));

        $response->assertStatus(\Illuminate\Http\Response::HTTP_OK);
        $response->assertViewIs('posts.show');
        $response->assertSee($post->name);
    }

    /** @test */


    public function getShowPostRoute($id)
    {
        return route('posts.show', ['id' => $id]);
    }
}
