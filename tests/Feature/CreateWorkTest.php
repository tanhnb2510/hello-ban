<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Work;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateWorkTest extends TestCase
{
    /** @test  */
    public function authenticated_user_can_create_works()
    {
        $this->actingAs(User::factory()->create());
        $works = Work::factory()->create()->toArray();
        $response = $this->post($this->getCreateWorkRoute(),$works);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('work',$works);
        $response->assertRedirect(route('works.index'));
    }

    public function getCreateWorkRoute()
    {
        return route('works.store');
    }
}
