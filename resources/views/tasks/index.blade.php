
<table class="table">
    <tr>
        <th>id</th>
        <th>name</th>
        <th>content</th>
    </tr>
    @foreach($tasks as $task)
        <tr>
            <td>{{$task->id}}</td>
            <td>{{$task->name}}</td>
            <td>{{$task->content}}</td>
        </tr>
    @endforeach
</table>
