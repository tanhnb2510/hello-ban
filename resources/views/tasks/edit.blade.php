@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Edit Form</h2>
                <form action="{{route('tasks.update',$tasks->id)}}" method="post">
                    @method('PUT')
                    @csrf
                    <div class="cart-header">
                        <input type="text" name="name" placeholder="" value="{{$tasks->name}}">
                        @error('name')
                        <span>{{$message}}</span>
                        @enderror
                    </div>
                    <input type="text" name="content" placeholder="" value="{{$tasks->content}}">
                    @error('content')
                    <span>{{$message}}</span>
                    @enderror
                    <div class="form-group row">
                        <input type="submit" class="col-sm-2 form-control" value="Cập nhật">
                        <input type="reset" class="col-sm-2 form-control" value="Nhập lại">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


