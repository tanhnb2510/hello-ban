<?php

namespace App\Http\Controllers;

use App\Models\Work;
use Illuminate\Http\Request;

class WorkController extends Controller
{
    protected $works;

    public function __construct(Work $works)
    {
        $this->works = $works;
    }

    public function index()
    {
        $works = $this->works->all();
        return view('works.index', compact('works'));
    }

    public function store(Request $request)
    {
        $this->works->create($request->all());
        return redirect(route('works.index'));
    }
}
