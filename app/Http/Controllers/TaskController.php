<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    protected $tasks;

    public function __construct(Task $tasks)
    {
        $this->tasks = $tasks;
    }

    public function index()
    {
        $tasks = $this->tasks->all();
        return view('tasks.index', compact('tasks'));
    }

    public function store(StoreTaskRequest $request)
    {
        $this->tasks->create($request->all());
        return redirect(route('tasks.index'));
    }

    public function create(Request $request)
    {
        return view('tasks.create');
    }

    public function update(UpdateTaskRequest $request, $id)
    {
        $tasks = $this->tasks->findOrFail($id);
        $tasks->update($request->all());
        return redirect(route('tasks.index'));
    }

    public function edit($id)
    {
        $tasks = $this->tasks->findOrFail($id);
        return view('tasks.edit', compact('tasks'));
    }

    public function show($id)
    {
        $task = $this->tasks->findOrFail($id);
        return view('tasks.show', compact('task'));
    }
}
