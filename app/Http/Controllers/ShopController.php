<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreShopRequest;
use App\Models\Shop;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    protected $shops;
    public function __construct(Shop $shops)
    {
        $this->shops = $shops;
    }

    public function index()
    {
        $shops = $this->shops->all();
        return view('shops.index',compact('shops'));
    }

    public function store(StoreShopRequest $request)
    {
        $this->shops->create($request->all());
        return redirect(route('shops.index'));
    }

    public function create()
    {
        return view('shops.create');
    }

    public function show($id)
    {
        $shop = $this->shops->findOrFail($id);
        return view('shops.show',compact('shop'));
    }

    public function update(Request $request ,$id)
    {
        $shop = $this->shops->findOrFail($id);
        $shop->update($request->all());
        return redirect()->route('shops.index');
    }

    public function edit($id)
    {
        return view('shops.edit');
    }
}
